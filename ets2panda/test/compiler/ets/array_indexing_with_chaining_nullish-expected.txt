{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 11
              },
              "end": {
                "line": 16,
                "column": 11
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 9
          },
          "end": {
            "line": 16,
            "column": 11
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 11
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "Identifier",
                          "name": "arr",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 5
                            },
                            "end": {
                              "line": 18,
                              "column": 8
                            }
                          }
                        },
                        "right": {
                          "type": "ArrayExpression",
                          "elements": [
                            {
                              "type": "ETSNewClassInstanceExpression",
                              "typeReference": {
                                "type": "ETSTypeReference",
                                "part": {
                                  "type": "ETSTypeReferencePart",
                                  "name": {
                                    "type": "Identifier",
                                    "name": "Object",
                                    "decorators": [],
                                    "loc": {
                                      "start": {
                                        "line": 18,
                                        "column": 34
                                      },
                                      "end": {
                                        "line": 18,
                                        "column": 40
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 34
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 41
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 34
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 41
                                  }
                                }
                              },
                              "arguments": [],
                              "loc": {
                                "start": {
                                  "line": 18,
                                  "column": 30
                                },
                                "end": {
                                  "line": 18,
                                  "column": 43
                                }
                              }
                            },
                            {
                              "type": "ETSNewClassInstanceExpression",
                              "typeReference": {
                                "type": "ETSTypeReference",
                                "part": {
                                  "type": "ETSTypeReferencePart",
                                  "name": {
                                    "type": "Identifier",
                                    "name": "A",
                                    "decorators": [],
                                    "loc": {
                                      "start": {
                                        "line": 18,
                                        "column": 48
                                      },
                                      "end": {
                                        "line": 18,
                                        "column": 49
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 48
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 50
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 48
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 50
                                  }
                                }
                              },
                              "arguments": [],
                              "loc": {
                                "start": {
                                  "line": 18,
                                  "column": 44
                                },
                                "end": {
                                  "line": 18,
                                  "column": 52
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 29
                            },
                            "end": {
                              "line": 18,
                              "column": 52
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 5
                          },
                          "end": {
                            "line": 18,
                            "column": 52
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 1
                        },
                        "end": {
                          "line": 18,
                          "column": 53
                        }
                      }
                    },
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "Identifier",
                          "name": "elem",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 5
                            },
                            "end": {
                              "line": 19,
                              "column": 9
                            }
                          }
                        },
                        "right": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "Identifier",
                            "name": "arr",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 33
                              },
                              "end": {
                                "line": 19,
                                "column": 36
                              }
                            }
                          },
                          "property": {
                            "type": "NumberLiteral",
                            "value": 0,
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 39
                              },
                              "end": {
                                "line": 19,
                                "column": 40
                              }
                            }
                          },
                          "computed": true,
                          "optional": true,
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 33
                            },
                            "end": {
                              "line": 19,
                              "column": 41
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 5
                          },
                          "end": {
                            "line": 19,
                            "column": 41
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 1
                        },
                        "end": {
                          "line": 19,
                          "column": 42
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "arr",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 8
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "TSArrayType",
              "elementType": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "Object",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 11
                      },
                      "end": {
                        "line": 18,
                        "column": 17
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 11
                    },
                    "end": {
                      "line": 18,
                      "column": 18
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 11
                  },
                  "end": {
                    "line": 18,
                    "column": 18
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 20
                },
                "end": {
                  "line": 18,
                  "column": 21
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 21
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "elem",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 5
                },
                "end": {
                  "line": 19,
                  "column": 9
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "Object",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 12
                    },
                    "end": {
                      "line": 19,
                      "column": 18
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 19,
                    "column": 12
                  },
                  "end": {
                    "line": 19,
                    "column": 20
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 19,
                  "column": 12
                },
                "end": {
                  "line": 19,
                  "column": 20
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 19,
                "column": 5
              },
              "end": {
                "line": 19,
                "column": 20
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 20,
      "column": 1
    }
  }
}
